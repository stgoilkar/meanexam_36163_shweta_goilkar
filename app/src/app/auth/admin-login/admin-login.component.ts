import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  email = ''
  password = ''

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private authService: AuthService) { }

  ngOnInit(): void {
  }


  onLogin() {
    if (this.email.length == 0) {
      this.toastr.error('please enter email')
    } else if (this.password.length == 0) {
      this.toastr.error('please enter password')
    } else {
      this.authService
        .adminlogin(this.email, this.password)
        .subscribe(response => {
          if (response['status'] == 'success') {
            const data = response['success']
            console.log(data)

            // cache the user info
            sessionStorage['token'] = data['token']
            sessionStorage['empName'] = data['empName']

            this.toastr.success(`Welcome ${data['empName']} to Admin panel of Employee Management System `)

            // goto the dashboard
            this.router.navigate(['/home'])

          } else {
            alert(response['error'])
          }
        })
    }
  }

}
