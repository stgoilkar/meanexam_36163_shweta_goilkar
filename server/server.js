const express = require('express')
const config = require('./config')
const jwt = require('jsonwebtoken')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')
const adminRoute = require('./routes/admin')
const empRoute = require('./routes/emp')

const app = express()
app.use(bodyParser.json())
app.use(cors())
app.use(morgan('combined'))

app.use((request, response, next) => {
    if (request.url == "/emp/signin" ||
        request.url == "/admin/signin") {
        next()
    } else {
        try {
            const token = request.headers['token']
            console.log(token);
            const data = jwt.verify(token, config.secret)
            request.id = data['id']
            next()
        } catch (ex) {
            response.status(401)
            response.send('You are not allowed to access')
        }
    }
})

app.use('/admin', adminRoute)
app.use('/emp', empRoute)

app.get('/', (request, response) => {
    response.send('Welcome to  Employee Managemen')
})

app.listen(8080, '0.0.0.0', () => {
    console.log("Server Started on port 8080")
})