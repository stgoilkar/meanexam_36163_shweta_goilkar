const mailer = require('nodemailer')
const config = require('./config')

function sendMail(email, subject, body, callback) {
    const transport = mailer.createTransport({
        service: 'gmail',
        auth: {
            user: config.emailUser,
            pass: config.emailPassword
        }
    })

    const options = {
        from: config.emailUser,
        to: email,
        subject: subject,
        html: body
    }

    transport.sendMail(options, callback)
}

module.exports = {
    sendMail: sendMail
}