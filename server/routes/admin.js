const { request, response } = require('express')
const express = require('express')
const db = require('../db')
const mailer = require("../mailer");
const config = require('../config')
const utils = require('../utils')
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')

const router = express.Router()


// -------------------------------------------------------------------------------------------------------------
//                                                  GET
// ---------------------------------------------------------------------------------------------------------------------


router.get('/meetingListAndMeetingStatus', (request, response) => {
    const statement = `select meetingDate,meetingInfo, meetingStatus from meeting where adminId=${request.id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})



router.get('/viewEmpQueries', (request, response) => {
    const statement = `select queryTitle,queryDesc,queryStatus,empId from emp_queries where queryStatus='not solved'`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})



// -------------------------------------------------------------------------------------------------------------
//                                                  Post
// ---------------------------------------------------------------------------------------------------------------------


router.post('/signin', (request, response) => {
    const { email, password } = request.body

    const statement = `select empId,empName,address, birth_date,gendor from employee where email='${email}' and password='${password}' and role='admin'`
    db.query(statement, (error, admins) => {
        if (error) {
            response.send({ status: 'Error', error: error })
        } else {
            if (admins.length == 0) {
                response.send({ status: 'error', error: 'Admin done not exist' })
            } else {
                const admin = admins[0]
                const token = jwt.sign({ id: admin['empId'] }, config.secret)
                response.send({
                    status: 'success',
                    success: {
                        empName: admin['empName'],
                        address: admin['address'],
                        birth_date: admin['birth_date'],
                        gender: admin['gendor'],
                        token: token
                    }
                })
            }
        }
    })
})


router.post("/addNewEmployee", (request, response) => {
    const { empName, email, password } = request.body;

    let body = `<h1>Welcome ${empName}
    <br>
    Your Email : '${email}'
    <br>
    Your Password :'${password}'`

    const statement = `insert into employee ( empName, email, password,role ) values ('${ empName}', '${email}', '${password}', 'employee')`;
    db.query(statement, (error, data) => {
        mailer.sendMail(email, "Employee Management", body, (error, info) => {
            console.log(error)
            console.log(info)
            response.send(utils.createReult(error, data));
        });
    });
});



router.post('/addNewMeetingDetails', (request, response) => {
    const { meetingDate, meetingInfo } = request.body

    const statement = `insert into meeting (meetingDate , meetingInfo,adminId,meetingStatus) values ('${meetingDate}','${meetingInfo}',${request.id},'not done')`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})


router.post('/searchEmpAndViewSalarySlip', (request, response) => {
    const { empId } = request.body
    const statement = `select noOfPresentDay , leaveCount, totalAmount,empId from salary_slip where empId=${empId} and adminId=${request.id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})


router.post('/searchEmpAndFindHisAttendanceOfMonth', (request, response) => {
    const { empId } = request.body
    const statement = `select (30-(select count(leaveId) from apply_leave where empId=${empId})) as attendace`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})



router.post('/generateSalarySlipForEmp', (request, response) => {
    const { empId } = request.body
    const salStatement = `insert into salary_slip (noOfPresentDay, leaveCount, totalAmount, adminId, empId) values ((select (30-(select count(leaveId) from apply_leave where empId=${empId})) as attendace),30-(select (30-(select count(leaveId) from apply_leave where empId=${empId})) as attendace),(select (30-(select count(leaveId) from apply_leave where empId=${empId})) as attendace)*800,${request.id},${empId})`
    db.query(salStatement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})


// -------------------------------------------------------------------------------------------------------------
//                                                  PUT
// ---------------------------------------------------------------------------------------------------------------------



router.put('/viewAndApproveEmpAppliedLeave', (request, response) => {
    const { empId } = request.body

    const statement = `select leaveDate,leavePurpose, leaveStatus,empId from apply_leave where leaveStatus='pending'`
    db.query(statement, (error, data) => {
        const updatestatement = `update apply_leave set leaveStatus='approved' where empId=${empId}`
        db.query(updatestatement, (error, data) => {
            response.send(utils.createResult(error, data))
        })
    })
})



// -------------------------------------------------------------------------------------------------------------
//                                                  DELETE
// ---------------------------------------------------------------------------------------------------------------------

module.exports = router