const { request, response } = require('express')
const express = require('express')
const db = require('../db')
const config = require('../config')
const utils = require('../utils')
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')

const router = express.Router()


// -------------------------------------------------------------------------------------------------------------
//                                                  GET
// ---------------------------------------------------------------------------------------------------------------------

router.get('/viewleaveStatus', (request, response) => {

    const statement = `select leaveDate,leavePurpose, leaveStatus from apply_leave where empId=${request.id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})


router.get('/meetingDetailsList', (request, response) => {
    const statement = `select meetingDate,meetingInfo, meetingStatus from meeting`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})


router.get('/viewSalarySlip', (request, response) => {
    const statement = `select noOfPresentDay, leaveCount , totalAmount from salary_slip where empId=${request.id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})



router.get('/knowappliedQueryStatus', (request, response) => {
    const statement = `select queryTitle,queryDesc,queryStatus from emp_queries where empId=${request.id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})


// -------------------------------------------------------------------------------------------------------------
//                                                  Post
// ---------------------------------------------------------------------------------------------------------------------

router.post('/signin', (request, response) => {
    const { email, password } = request.body

    const statement = `select empId,empName,address, birth_date,gendor from employee where email='${email}' and password='${password}' and role='employee'`
    db.query(statement, (error, emps) => {
        if (error) {
            response.send({ status: 'Error', error: error })
        } else {
            if (emps.length == 0) {
                response.send({ status: 'error', error: 'Employee done not exist' })
            } else {
                const emp = emps[0]
                const token = jwt.sign({ id: emp['empId'] }, config.secret)
                response.send({
                    status: 'success',
                    success: {
                        empName: emp['empName'],
                        address: emp['address'],
                        birth_date: emp['birth_date'],
                        gender: emp['gendor'],
                        token: token
                    }
                })
            }
        }
    })
})

router.post('/applyleave', (request, response) => {
    const { leaveDate, leavePurpose } = request.body

    const statement = `insert into apply_leave (leaveDate,leavePurpose,empId,leaveStatus) values ('${leaveDate}','${leavePurpose}',${request.id},'pending')`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})



router.post('/addNewQueries', (request, response) => {
    const { queryTitle, queryDesc } = request.body

    const statement = `insert into emp_queries (queryTitle,queryDesc,empId,queryStatus) values ('${queryTitle}','${queryDesc}',${request.id},'not solved')`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

// -------------------------------------------------------------------------------------------------------------
//                                                  PUT
// ---------------------------------------------------------------------------------------------------------------------

router.put('/updateProfile', (request, response) => {
    const { empName, address, birth_date, gendor } = request.body

    const statement = `update employee set empName='${empName}',address='${address}',birth_date='${birth_date}',gendor='${gendor}' where empId=${request.id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})


// -------------------------------------------------------------------------------------------------------------
//                                                  DELETE
// ---------------------------------------------------------------------------------------------------------------------

module.exports = router